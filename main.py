from selenium import webdriver
import time

chrome_browser = webdriver.Chrome("./chromedriver_win32/chromedriver.exe")
chrome_browser.maximize_window()

chrome_browser.get("https://demo.seleniumeasy.com/basic-first-form-demo.html")

if "Selenium Easy Demo" in chrome_browser.title:
    enter_message_field = chrome_browser.find_element_by_id("user-message")
    enter_message_field.send_keys("Heyyo, its Seyma")

    show_message_button = chrome_browser.find_element_by_class_name("btn-default")
    show_message_button.click()

    your_message = chrome_browser.find_element_by_css_selector("#user-message > #display")
    if your_message.get_attribute("innerHTML") == "Heyyo, its Seyma":
        print("Message entered")
    chrome_browser.close()

     
    
